use std::fs;
use std::sync::mpsc;
use std::thread;
use std::sync::Arc;
use std::sync::Mutex;

#[derive(Copy, Clone)]
struct Coordinates {
    x: f64,
    y: f64,
}

fn main() {
    // Settings
    const N_PIXELS_X: usize = 500;
    const N_PIXELS_Y: usize = 500;
    const MAX_ITERATIONS: u32 = 1000;
    const BAILOUT: f64 = 100.0;
    const BAILOUT_SQ: f64 = BAILOUT * BAILOUT;
    const BOTTOM_LEFT: Coordinates = Coordinates { x: -2.0, y: -1.5 };
    const TOP_RIGHT: Coordinates = Coordinates { x: 0.5, y: 1.5 };
    const NTHREADS: usize = 4;
    let mut channels = Vec::new();
    let mut output = String::from(format!("P1 {} {}\n", N_PIXELS_X, N_PIXELS_Y));

    // Actual iteration
    fn in_mandel(c_re: f64, c_im: f64) -> u32 {
        let mut z_re = c_re;
        let mut z_im = c_im;
        let mut z_re_new;
        let mut z_im_new;
        for i in 0..MAX_ITERATIONS {
            z_re_new = z_re * z_re - z_im * z_im + c_re;
            z_im_new = 2.0 * z_re * z_im + c_im;
            z_re = z_re_new;
            z_im = z_im_new;
            if (z_re * z_re + z_im * z_im) > BAILOUT_SQ {
                return i;
            }
        }
        return 1;
    }

    // Spawn threads that will receive the c_im coordinate and a channel to send the results to
    let (tx_row, rx_row) = mpsc::channel::<(f64, mpsc::Sender<[u32; N_PIXELS_X]>)>();
    let rx_row = Arc::new(Mutex::new(rx_row));
    for _ in 0..NTHREADS {
        let rxi_row = Arc::clone(&rx_row);
        thread::spawn(move || {
            loop {
                let (c_im, tx) = match rxi_row.lock().unwrap().recv() {
                    Ok(val) => val,
                    Err(_) => break,
                };
                let mut row = [1; N_PIXELS_X];
                for ix in 0..N_PIXELS_X {
                    let c_re = BOTTOM_LEFT.x
                        + (ix as f64) * (TOP_RIGHT.x - BOTTOM_LEFT.x) / (N_PIXELS_X as f64);
                    row[ix] = in_mandel(c_re, c_im);
                }
                tx.send(row).unwrap();
            }
        });
    }

    // Printout rows from channels
    fn print_mandel(output: &mut String, channels: &Vec<mpsc::Receiver<[u32; N_PIXELS_X]>>) {
        for rx in channels {
            let row = rx.recv().unwrap();
            for in_mandel in row.iter() {
                if *in_mandel == 1 {
                    output.push('1');
                } else {
                    output.push('0');
                }
            }
            output.push('\n');
        }
    }

    // loop over rows, submit and print
    for iy in 0..N_PIXELS_Y {
        let c_im = TOP_RIGHT.y - (iy as f64) * (TOP_RIGHT.y - BOTTOM_LEFT.y) / (N_PIXELS_Y as f64);
        let (tx, rx) = mpsc::channel();
        channels.push(rx);

        tx_row.send((c_im, tx)).unwrap();

        if channels.len() == NTHREADS {
            print_mandel(&mut output, &channels);
            channels.clear();
        }
    }
    print_mandel(&mut output, &channels);
    fs::write("mandel.p1", output).unwrap();
}
