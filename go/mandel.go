package main

import (
	"fmt"
	"io/ioutil"
	"math"
)

func abs(z complex128) float64 {
	// cmplx.Abs is slow - probably for similar reasons as the one in c++:
	// https://stackoverflow.com/questions/53518381/stdabsstdcomplex-too-slow
	return math.Sqrt(float64(real(z)*real(z) + imag(z)*imag(z)))
}

func pixel_to_complex(ix, iy int, config MandelConfig) complex128 {
	dx := (config.top_right.X - config.bottom_left.X) / float64(config.n_pixels_x)
	dy := (config.top_right.Y - config.bottom_left.Y) / float64(config.n_pixels_y)
	c := complex(
		config.bottom_left.X+float64(ix)*dx,
		config.top_right.Y-float64(iy)*dy,
	)
	return complex128(c)
}

func in_mandel(ix, iy int, config MandelConfig) (bool){
	c := pixel_to_complex(ix, iy, config)
	z := c
	for i := 0; i < config.max_iterations; i++ {
		z = z*z + c
		if abs(z) > config.bailout {
			return false
		}
	}
	return true
}

type MandelResult struct {
	pixels []bool
	iy int
}

type Coordinates struct {
	X float64
	Y float64
}

type MandelConfig struct {
	n_pixels_x     int
	n_pixels_y     int
	bottom_left    Coordinates
	top_right      Coordinates
	max_iterations int
	bailout        float64
}

func worker(row chan MandelResult, res chan MandelResult, config MandelConfig) {
	for mr := range row {
		pixels := mr.pixels
		iy := mr.iy
		for ix := 0; ix < config.n_pixels_x; ix++ {
			pixels[ix] = in_mandel(ix, iy, config)
		}
		res <- MandelResult{pixels:pixels, iy:iy}
	}
}

func main() {
	// settings
	const nthreads = 4
	const chan_size = 100
	config := MandelConfig{
		n_pixels_x:     500,
		n_pixels_y:     500,
		max_iterations: 1000,
		bailout:        100.,
		bottom_left:    Coordinates{-2, -1.5},
		top_right:      Coordinates{0.5, 1.5},
	}

	// start threads
	next_row := make(chan MandelResult, chan_size)
	res := make(chan MandelResult, chan_size)
	for it := 0; it < nthreads; it++ {
		go worker(next_row, res, config)
	}

	// buffer to get rows into the right order
	img_slice := make([][]bool, chan_size)
	for is := range(img_slice) {
		img_slice[is] = make([]bool, config.n_pixels_x)
	}

	// output byte array with header
	output := []byte(fmt.Sprintf("P1\n%d %d\n", config.n_pixels_x, config.n_pixels_y))

	// this function will collect the pixel values from the result channel
	// and append them to the output
	write_output := func(nrows, iy int) {
		for ir := 0; ir < nrows; ir++ {
			mr := <- res
			for ix, v := range mr.pixels {
				img_slice[mr.iy - (iy - nrows + 1)][ix] = v
			}
		}
		for _, pixels := range img_slice {
			for _, v := range pixels {
				if v {
					output = append(output, '1')
				} else {
					output = append(output, '0')
				}
			}
			output = append(output, '\n')
		}
	}

	// main loop: submit rows, collect and write results
	nrows := 0
	iy := 0
	for ; iy < config.n_pixels_y; iy++ {
		next_row <- MandelResult{iy:iy, pixels:img_slice[nrows]}
		nrows++
		if nrows == chan_size {
			write_output(nrows, iy)
			nrows = 0
		}
	}
	close(next_row)
	write_output(nrows, iy)

	ioutil.WriteFile("mandel.p1", output, 0644)
}
