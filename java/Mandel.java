import java.awt.*;
import javax.swing.*;

public class Mandel extends Canvas {

    public int n_pixels_x = 500;
    public int n_pixels_y = 500;
    public int maxit = 1000;
    public float bailout = 100f;
    public float bailout_sq = bailout*bailout;
    public float[] bottom_left = {-2f, -1.5f};
    public float[] top_right = {0.5f, 1.5f};
    public float delta_x = top_right[0] - bottom_left[0];
    public float delta_y = top_right[1] - bottom_left[1];

    public int in_mandel(float c_re, float c_im) {
        float z_re = c_re;
        float z_im = c_im;
        float z_re_new;
        float z_im_new;
        for (int i=0; i<maxit; i++) {
            z_re_new = z_re*z_re - z_im*z_im + c_re;
            z_im_new = 2*z_re*z_im + c_im;
            z_re = z_re_new;
            z_im = z_im_new;
            if ((z_re*z_re + z_im*z_im) > bailout_sq) {
                return 0;
            }
        }
        return 1;
    }

    public void paint(Graphics graphics) {
        graphics.setColor(Color.black);
        long startTime = System.nanoTime();
        for (int i=0; i<n_pixels_x; i++) {
            for (int j=0; j<n_pixels_y; j++) {
                float c_re = bottom_left[0] + (float)i*delta_x/(float)n_pixels_x;
                float c_im = top_right[1] - (float)j*delta_y/(float)n_pixels_y;
                if (in_mandel(c_re, c_im) == 1) {
                    graphics.drawLine(i, j, i, j);
                }
            }
        }
        System.out.println("Took " + (System.nanoTime() - startTime)/1000000000. + " seconds");
    }

    public static void main(String[] args) {
        Mandel canvas = new Mandel();
        JFrame frame = new JFrame();
        frame.setSize(canvas.n_pixels_x, canvas.n_pixels_y);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(canvas);
        frame.setVisible(true);
    }
}
