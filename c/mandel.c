#include "stdio.h"

struct coordinates {
  float x, y;
};

int in_mandel(struct coordinates c, int max_iterations, float bailout) {
  float c_re = c.x;
  float c_im = c.y;
  float z_re = c_re;
  float z_im = c_im;
  float z_re_new;
  float z_im_new;
  float bailout_sq = bailout*bailout;
  for (int i=0; i<max_iterations; i++) {
    // z_{n+1} = z_{n}^2 + c
    z_re_new = z_re*z_re - z_im*z_im + c_re;
    z_im_new = 2*z_re*z_im + c_im;
    z_re = z_re_new;
    z_im = z_im_new;
    if ((z_re*z_re + z_im*z_im) > bailout_sq) {
      return 0;
    }
  }
  return 1;
}

struct coordinates convert_from_pixel(int x, int y,
                                      int n_pixels_x, int n_pixels_y,
                                      struct coordinates bottom_left,
                                      struct coordinates top_right) {
  struct coordinates c;
  c.x = bottom_left.x + x*(top_right.x - bottom_left.x)/(float)n_pixels_x;
  c.y = top_right.y - y*(top_right.y - bottom_left.y)/(float)n_pixels_y;
  return c;
}

int main () {
  int n_pixels_x = 500;
  int n_pixels_y = 500;
  int max_iterations = 1000;
  float bailout = 100.;
  struct coordinates bottom_left = {-2. , -1.5};
  struct coordinates top_right = {0.5 ,  1.5};

  // write P1 header
  FILE* fp = fopen("mandel.p1", "w");
  fprintf(fp, "P1\n");
  fprintf(fp, "%d %d\n", n_pixels_x, n_pixels_y);

  // write pixels
  struct coordinates c;
  for (int y=0; y<n_pixels_y; y++) {
    for (int x=0; x<n_pixels_x; x++) {
      c = convert_from_pixel(x, y, n_pixels_x, n_pixels_y, bottom_left, top_right);
      fprintf(fp, "%d ", in_mandel(c, max_iterations, bailout));
    }
    fprintf(fp, "\n");
  }
}
