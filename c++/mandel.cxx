#include <fstream>
#include <complex>

class Mandel {
public:

  Mandel(int nx=500, int ny=500, int maxit=1000, float bailout=100.,
         float bottom_left_x=-2., float bottom_left_y=-1.5,
         float top_right_x=0.5, float top_right_y=1.5)
    : m_nx(nx), m_ny(ny), m_maxit(maxit), m_bailout(bailout),
      m_bottom_left_x(bottom_left_x), m_bottom_left_y(bottom_left_y),
      m_top_right_x(top_right_x), m_top_right_y(top_right_y) { }

  // std::complex seems to be super slow ...
  // faster method reimplemented below in MandelFast
  virtual int in_mandel(std::complex<float> c) {
    m_z = c;
    for (int i=0; i<m_maxit; i++) {
      m_z = m_z*m_z + c;
      if (abs(m_z) > m_bailout) {
        return 0;
      }
    }
    return 1;
  }

  std::complex<float> get_position(int px, int py) {
    float re = m_bottom_left_x + (float)px*(m_top_right_x - m_bottom_left_x)/(float)m_nx;
    float im = m_top_right_y - (float)py*(m_top_right_y - m_bottom_left_y)/(float)m_ny;
    return std::complex<float> {re, im};
  }

  void draw(const char* fname) {
    std::ofstream of(fname);
    of << "P1" << std::endl;
    of << m_nx << " " << m_ny << std::endl;
    for (int py=0; py<m_nx; py++) {
      for (int px=0; px<m_nx; px++) {
        if (in_mandel(get_position(px, py))) {
          of << "1 ";
        } else {
          of << "0 ";
        }
      }
      of << std::endl;
    }
  }

  const int m_nx;
  const int m_ny;
  const int m_maxit;
  const float m_bailout;
  const float m_bottom_left_x;
  const float m_bottom_left_y;
  const float m_top_right_x;
  const float m_top_right_y;

protected:
  std::complex<float> m_c;
  std::complex<float> m_z;
};


class MandelFast : public Mandel {
public:

  using Mandel::Mandel;

  // much faster with manual calculation
  int in_mandel(std::complex<float> c) {
    float c_re = c.real();
    float c_im = c.imag();
    float z_re = c_re;
    float z_im = c_im;
    float z_re_new;
    float z_im_new;
    float bailout_sq = m_bailout*m_bailout;
    for (int i=0; i<m_maxit; i++) {
      // z_{n+1} = z_{n}^2 + c
      z_re_new = z_re*z_re - z_im*z_im + c_re;
      z_im_new = 2*z_re*z_im + c_im;
      z_re = z_re_new;
      z_im = z_im_new;
      if ((z_re*z_re + z_im*z_im) > bailout_sq) {
        return 0;
      }
    }
    return 1;
  }
};

int main() {
  MandelFast mandel;
  mandel.draw("mandel.p1");
  return 0;
}
