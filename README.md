# The Mandelbrot set in different programming languages
The code examples are neither meant to be as short as possible, nor clever (but
for some examples i played around to get them reasonably fast). Some examples
give graphical output, some just dump a ppm file or render to ascii art.

If you are interested in a bit more "code golf style" set of examples have a look at

https://codegolf.stackexchange.com/questions/23423/mandelbrot-image-in-every-language
