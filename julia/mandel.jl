#!/usr/bin/env -S julia -i

function mandel(c_re, c_im; max_iterations=1000, bailout=100.)
    z_re = c_re
    z_im = c_im
    bailout_sq = bailout*bailout
    for i=0:max_iterations
        z_re, z_im = (
            z_re*z_re - z_im*z_im + c_re,
            2*z_re*z_im + c_im
        )
        if (z_re*z_re + z_im*z_im) > bailout_sq
            return i
        end
    end
    return 0
end

# Plots.jl takes forever to load
# see https://github.com/JuliaPlots/Plots.jl/issues/917
# using Plots
# gr()

# use PyPlot instead
using PyPlot

xs = range(-2., 0.5, length=500)
ys = range(-1.5, 1.5, length=500)
X = repeat(reshape(xs, 1, :), length(ys), 1)
Y = repeat(ys, 1, length(xs))
@time Z = map(mandel, X, Y)
pcolormesh(
    X, Y, Z,
    cmap="inferno", norm=matplotlib[:colors][:LogNorm]()
)
