#!/bin/bash

# actually, this is more "bc" than bash ;)
# ... and incredibly slow

NX=50
NY=50
MAXIT=10
BAILOUT=10.
BOTTOM_LEFT_X=-2.
BOTTOM_LEFT_Y=-1.5
TOP_RIGHT_X=0.5
TOP_RIGHT_Y=1.5

for ((py=0;py<$NY;py++))
do
    line=""
    for ((px=0;px<$NX;px++))
    do
        line+=$(cat <<EOF | bc -l -q
define in_mandel() {
cre = $BOTTOM_LEFT_X + $px*(${TOP_RIGHT_X} - ${BOTTOM_LEFT_X})/$NX
cim = $TOP_RIGHT_Y - $py*(${TOP_RIGHT_Y} - ${BOTTOM_LEFT_Y})/$NY
zre = cre
zim = cim
for (i=0;i<$MAXIT;i++) {
    zrenew = zre*zre - zim*zim + cre
    zimnew = 2*zre*zim + cim
    zre = zrenew
    zim = zimnew
    if ((zre*zre + zim*zim) > ${BAILOUT}*${BAILOUT}) {
        return 0
    }
}
if (i==$MAXIT) {
    return 1
}
}
in_mandel()
EOF
)
    done
    echo $line | sed 's/1/* /g' | sed 's/0/  /g'
done
