#!/usr/bin/env python

import time

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

import pyximport;
pyximport.install()
from mandel import mandel

c_re, c_im = np.meshgrid(
    np.linspace(-2., 0.5, 500),
    np.linspace(-1.5, 1.5, 500)
)

vmandel = np.vectorize(
    lambda x_re, x_im : mandel(
        x_re, x_im, max_iterations=1000, bailout=100.
    )
)
start = time.time()
mandel_iterations = vmandel(c_re, c_im)
print("Took {} seconds".format(time.time()-start))

c = plt.pcolormesh(
    c_re, c_im, mandel_iterations,
    norm=LogNorm(), cmap="inferno"
)
plt.colorbar(c)
plt.show()
