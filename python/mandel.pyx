def mandel(float c_re, float c_im, int max_iterations, float bailout):
    cdef int i
    cdef float z_re = c_re
    cdef float z_im = c_im
    cdef float z_re_new, z_im_new
    cdef float bailout_sq = bailout*bailout
    for i in range(max_iterations):
        z_re_new = z_re*z_re - z_im*z_im + c_re
        z_im_new = 2*z_re*z_im + c_im
        z_re = z_re_new
        z_im = z_im_new
        if (z_re*z_re + z_im*z_im) > bailout_sq:
            return i
    return 0
