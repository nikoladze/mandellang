#!/usr/bin/env python

import time

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

def mandel(c_re, c_im, max_iterations=1000, bailout=100.):
    start = time.time()
    z_re, z_im = np.array(c_re), np.array(c_im)
    iterations = np.zeros(shape=c_re.shape, dtype=np.int)
    for i in range(max_iterations):
        m = (z_re*z_re + z_im*z_im) < bailout**2
        z_re[m], z_im[m] = (
            z_re[m]*z_re[m] - z_im[m]*z_im[m] + c_re[m],
            2*z_re[m]*z_im[m] + c_im[m]
        )
        iterations[(~m) & (iterations == 0)] = i
    print("Took {} seconds".format(time.time()-start))
    return iterations

c_re, c_im = np.meshgrid(
    np.linspace(-2., 0.5, 500),
    np.linspace(-1.5, 1.5, 500)
)

c = plt.pcolormesh(
    c_re, c_im, mandel(c_re, c_im),
    norm=LogNorm(), cmap="inferno"
)
plt.colorbar(c)
plt.show()
