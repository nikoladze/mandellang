! https://www.fortran90.org/src/rosetta.html has another example of a mandelbrot set in fortran
! (and a few nice examples how numpy stuff would look in fortran)

! need to write this into module to have "explicit interface"
! for elemental function (although i don't use the elemental feature here atm)
module mandel
  implicit none
  real :: bailout = 100.
  integer :: maxit = 1000
  contains
  integer elemental function in_mandel(c)
    implicit none
    complex, intent(in) :: c
    complex :: z
    z = c
    do in_mandel=0,maxit-1
       z = z**2 + c
       if (abs(z) > bailout) then
          return
       end if
    end do
  end function in_mandel
end module mandel

program main
  use mandel, only: in_mandel, maxit
  integer :: i, j, niter
  integer :: nx = 500
  integer :: ny = 500
  complex :: bottom_left = (-2., -1.5)
  complex :: top_right = (0.5, 1.5)
  complex c
  complex range
  range = top_right - bottom_left
  open (20, file='mandel.p1', status='replace')
  write(20, "(a)") "P1"
  write(20, "(i5,i5)") nx, ny
  do i=0,nx-1
     do j=0,ny-1
        c = cmplx(real(bottom_left) + real(j)*real(range)/real(nx), aimag(top_right) - real(i)*aimag(range)/real(ny))
        niter = in_mandel(c)
        if (niter == maxit) then
           write(20, fmt="(i2)", advance="no") 1
        else
           write(20, fmt="(i2)", advance="no") 0
        end if
     end do
     write(20, *) ""
  end do
end program main
